Higher-Lower Game
===========

This game is a work in progress.
====

This is a simple graphical higher-lower game based off thepeckingbird's game that is written in JavaScript.
The game is still currently a work in progress as I am currently ironing out some issues with it. Feel free to fork it and send a pull request if you believe you have fixed any errors.

To run the game
====

While the game itself does run, there are a few logical errors that prevent it from having the desired function; therefore building it should be done solely for debugging purposes at this moment.

You will need the Java Runtime Environment and the Java SDK to compile it. Simply use the javac tool to compile from commandline.
Once compiled you can use java to run the program, which will launch a graphical user interface.

[![endorse](http://api.coderwall.com/jamobox/endorsecount.png)](http://coderwall.com/jamobox)