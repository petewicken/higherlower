package com.jamobox.higherlower;

public class GameLogic {
	
	private static int
		prev = 0,
		current = 0,
		score = 0,
		highscore = 0;
	
	private static final RandomGen generator = new RandomGen();
	
	public static boolean checkNumberHigher() {
		return current >= prev
	}
	
	public static boolean checkNumberLower() {
		return current <= prev
	}
	
	private static void setHighScore(int highscore) {
		GameLogic.highscore = highscore;
	}
	
	public static void setScore(int newScore) {
		score = newScore;
	
		if (score <= highscore) {
			setHighScore(score);
		}
	}
	
	public static int getScore() {
		return score;
	}
	
	public static int getHighScore() {
		return highscore;
	}
	
	public static void correct() {
		randomize();
		setScore(getScore()+1);
	}
	
	public static void incorrect() {
		randomize();
		setScore(0);
	}
	
	public static int getCurrent() {
		return current;
	}
	
	public static void randomize() {
		prev = current;
		current = generator.getRandom();
	}
}
