package com.jamobox.higherlower;

import com.jamobox.higherlower.gui.Swing;

public class HigherLower {
	public static final String version = "v1.0"; 
	private static final Swing swing = new Swing();
	
	public static void main(String[] args) {
		swing.runFrame();
	}
	
	public static String getVersion() {
		return version;
	}
}
