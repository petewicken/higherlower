package com.jamobox.higherlower;

import java.util.Random;

public class RandomGen {
	
	private Random random;
	private int randomNum;
	private int highBound;
	
	public RandomGen() {
		highBound = 10;
	}
	
	public void setHighBound(int highBound) {
		this.highBound = highBound;
	}
	
	public int getRandom() {
		random = new Random();
		randomNum = random.nextInt(highBound+1);
		
		return randomNum;
	}
}
