package com.jamobox.higherlower.gui;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.jamobox.higherlower.GameLogic;
import com.jamobox.higherlower.HigherLower;

public class Swing {

	private volatile SwingConfig conf = new SwingConfig();
	
	public void runFrame() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GameLogic.randomize();
				conf.setTitle("HigherLower Version "+HigherLower.version);
				
				SwingFrame frame = new SwingFrame(conf.getTitle());
				
				frame.setSize(conf.getWidth(), conf.getHeight());
				frame.setLocation(conf.getXPos(), conf.getYPos());
				frame.setResizable(false);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
				
				SwingCenterPanel.setNumberLabel(GameLogic.getCurrent());
			}
		});
	}
	
}
