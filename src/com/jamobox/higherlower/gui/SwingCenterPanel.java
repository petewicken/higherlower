package com.jamobox.higherlower.gui;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class SwingCenterPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private static JLabel currentLabel;;
	
	public SwingCenterPanel() {
		currentLabel = new JLabel();
		
		this.add(currentLabel, BorderLayout.CENTER);
	}
	
	public static void setNumberLabel(int current) {
		currentLabel.setText(Integer.toString(current));
	}
	
	public static JLabel getNumberLabel(){
		return currentLabel;
	}
	
}
