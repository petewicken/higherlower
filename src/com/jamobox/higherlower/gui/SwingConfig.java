package com.jamobox.higherlower.gui;

public class SwingConfig {

		private int width, height, xPos, yPos;
		private String title;
		
		public SwingConfig() {
			this.height = 150;
			this.width = 300;
			this.xPos = 0;
			this.yPos = 0;
			this.title = "My Frame";
		}
		
		public void setHeight(int height) {
			this.height = height;
		}
		
		public void setWidth(int width) {
			this.width = width;
		}

		public void setXPos(int xPos) {
			this.xPos = xPos;
		}

		public void setYPos(int yPos) {
			this.yPos = yPos;
		}
		
		public void setTitle(String title) {
			this.title = title;
		}

		public int getHeight() {
			return height;
		}

		public int getWidth() {
			return width;
		}

		public int getXPos() {
			return xPos;
		}
		
		public int getYPos() {
			return yPos;
		}
		
		public String getTitle() {
			return title;
		}
		
}
