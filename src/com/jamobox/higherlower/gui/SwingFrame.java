package com.jamobox.higherlower.gui;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class SwingFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	public SwingFrame(String title) {
		super(title);
		constructFrame();
	}
	
	public void constructFrame() {
		
		setLayout(new BorderLayout());
		
		SwingUpperPanel pnlUpper = new SwingUpperPanel();
		SwingLowerPanel pnlLower = new SwingLowerPanel();
		SwingCenterPanel pnlCenter = new SwingCenterPanel();
		JPanel pane = new JPanel(new BorderLayout());
		
		Container c = getContentPane();
		
		c.add(pane);
		pane.add(pnlCenter, BorderLayout.CENTER);
		pane.add(pnlUpper, BorderLayout.NORTH);
		pane.add(pnlLower, BorderLayout.SOUTH);
	}

}
