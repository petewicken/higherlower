package com.jamobox.higherlower.gui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jamobox.higherlower.gui.buttons.HigherAction;
import com.jamobox.higherlower.gui.buttons.LowerAction;

public class SwingLowerPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private final JButton btnHigher = new JButton("Higher");
	private final JButton btnLower = new JButton("Lower");
	private final HigherAction higherAction = new HigherAction();
	private final LowerAction lowerAction = new LowerAction();
	private JLabel lblFeedback;
	
	public SwingLowerPanel() {
		
		JPanel pane = new JPanel(new BorderLayout());
		lblFeedback = new JLabel();
		
		this.add(btnLower, BorderLayout.WEST);
		this.add(btnHigher, BorderLayout.EAST);
		this.add(pane, BorderLayout.SOUTH);
		pane.add(lblFeedback);
		
		btnHigher.addActionListener(higherAction);
		btnLower.addActionListener(lowerAction);
	}
	
	public void setFeedbackText(String text) {
		this.lblFeedback.setText(text);
	}

}
