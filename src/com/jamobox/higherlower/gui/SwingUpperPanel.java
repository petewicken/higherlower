package com.jamobox.higherlower.gui;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jamobox.higherlower.GameLogic;

public class SwingUpperPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private static final String highscoreText = "Highscore: ";
	private static final String scoreText = "Score: ";
	private static JLabel lblScore;
	private static JLabel lblHighScore;
	private JLabel lblPipe = new JLabel("     |     ");
	
	public SwingUpperPanel() {
		lblScore = new JLabel(scoreText+GameLogic.getScore());
		lblHighScore = new JLabel(highscoreText+GameLogic.getHighScore());
		
		this.add(lblScore, BorderLayout.BEFORE_FIRST_LINE);
		this.add(lblPipe, BorderLayout.NORTH);
		this.add(lblHighScore, BorderLayout.AFTER_LINE_ENDS);
	}
	
	public void setDisplayScore(int score) {
		lblScore.setText(scoreText+Integer.toString(score));
	}

	public void setDisplayHighscore(int highscore) {
		lblHighScore.setText(highscoreText+Integer.toString(highscore));
	}
}
