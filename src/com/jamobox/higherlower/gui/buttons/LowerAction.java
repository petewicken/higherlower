package com.jamobox.higherlower.gui.buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.jamobox.higherlower.GameLogic;
import com.jamobox.higherlower.gui.SwingCenterPanel;
import com.jamobox.higherlower.gui.SwingLowerPanel;
import com.jamobox.higherlower.gui.SwingUpperPanel;

public class LowerAction implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {

		SwingUpperPanel upperPanel = new SwingUpperPanel();
		SwingLowerPanel lowerPanel = new SwingLowerPanel();
		
		if (GameLogic.checkNumberLower()) {
			GameLogic.correct();
			lowerPanel.setFeedbackText("Correct!");
			SwingCenterPanel.setNumberLabel(GameLogic.getCurrent());
			upperPanel.setDisplayScore(GameLogic.getScore());
			upperPanel.setDisplayHighscore(GameLogic.getHighScore());
		} else {
			GameLogic.incorrect();
			lowerPanel.setFeedbackText("Incorrect!");
			SwingCenterPanel.setNumberLabel(GameLogic.getCurrent());
			upperPanel.setDisplayScore(GameLogic.getScore());
			upperPanel.setDisplayHighscore(GameLogic.getHighScore());
		}
	}

}
